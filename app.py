from flask import Flask, render_template, request, redirect, url_for
import sqlite3

app = Flask(__name__)
nav = {'Main':'index','Names List':'names_list'}

@app.route('/', methods=['POST', 'GET'])
@app.route('/index', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        exist = False
        input_name = request.form['input_name']
        conn = sqlite3.connect('database.db')
        cur = conn.cursor()
        names_list = cur.execute(f"SELECT input_name FROM names_list WHERE input_name = '{input_name}'").fetchall()

        if len(names_list) > 0:
            exist = True
        else:
            cur.execute(f"INSERT INTO names_list (input_name) VALUES ('{input_name}')")
            conn.commit()
        
        conn.close()
        return render_template('sayhello.html', title='Hello', nav=nav, exist=exist, input_name=input_name)

    return render_template('index.html', title='Main', nav=nav)

@app.route('/names_list')
def names_list():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    names_list = cur.execute("SELECT * FROM names_list").fetchall()
    conn.close()
    return render_template('namelist.html', title='Names List', nav=nav, names_list=names_list)

@app.route('/delete/<id>', methods=['POST'])
def delete_name(id):
    print(id)
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute(f"DELETE FROM names_list WHERE id={id}").fetchall()
    conn.commit()
    conn.close()
    return redirect(url_for('names_list'))

if __name__ == '__main__':
    app.run()
