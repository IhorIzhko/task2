import sqlite3

def create_database():  
    conn = sqlite3.connect('database.db')

    with open('schema.sql') as f:
        conn.executescript(f.read())

    conn.commit()
    conn.close()

if __name__ == '__main__':
    create_database()